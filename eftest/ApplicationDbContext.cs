﻿using Microsoft.AspNet.Identity.EntityFramework;
using Npgsql;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MoonMachine.Infrastructure.Identity {
    [DbConfigurationType(typeof(NpgSqlConfiguration))]
    public partial class ApplicationDbContext : IdentityDbContext<customuser> {
        public const string schemalabel = "dbo";
        public const string defaultdatabasekey = "DefaultConnection";

        public ApplicationDbContext() : base(defaultdatabasekey, throwIfV1Schema: false)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ApplicationDbContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>());
        }

        public ApplicationDbContext(string connectionstringkey) : base(connectionstringkey, throwIfV1Schema: false) {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ApplicationDbContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>());
        }

        /// <summary>
        /// used for pipeline construction
        /// </summary>
        /// <returns></returns>
        public static ApplicationDbContext Create() => new ApplicationDbContext(defaultdatabasekey);

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            // Global conventions for decimals.
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            // Just disable one to many cascade, can be selectively re-enabled if needed.
            // Trying to delete a principal will throw instead.
            // By default 1 to 0..1 don't allow cascade.
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new IdentityUserConfiguration());
        }

    }

    internal class IdentityUserConfiguration : EntityTypeConfiguration<customuser> {
        public IdentityUserConfiguration() {
            //Ignore Unused Fields
            Ignore(c => c.Email);
            Ignore(c => c.EmailConfirmed);
            
            HasKey(c => c.Id);
            HasIndex(c => c.Id);           

            HasIndex(c => c.PhoneNumber)
                .IsUnique();

            Property(x => x.PhoneNumber)
                .IsFixedLength()
                .HasMaxLength(16)
                .IsRequired();
            
            Property(x => x.lastpaymentmade)
                .IsRequired();

            Property(x => x.DateJoined)
                .IsRequired();
        }
    }

    public class NpgSqlConfiguration : DbConfiguration {
        public NpgSqlConfiguration() {
            var providername = "Npgsql";
            SetProviderFactory(providername, NpgsqlFactory.Instance);
            SetProviderServices(providername, provider: NpgsqlServices.Instance);
            SetDefaultConnectionFactory(new NpgsqlConnectionFactory());
        }
    }
}
