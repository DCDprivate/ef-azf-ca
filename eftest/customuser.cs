﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MoonMachine.Infrastructure.Identity {

    public class customuser : IdentityUser {

        public customuser()
        {
        }

        //User Properties
        public bool IsSubscribed { get; set; }
        public bool IsSuperUser { get; set; }

        public DateTime lastpaymentmade { get; set; }

        /// <summary>
        /// Required
        /// </summary>
        public DateTime DateJoined { get; set; }

        //Overrides
        [Phone]
        public override string PhoneNumber { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<customuser> manager, string authenticationType) {
            // Value of authenticationType Must be Equals as defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Custom Columns for IdentityUser
            return userIdentity;
        }
    }
}
